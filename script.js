// Waing for document to be fully load before executing scripts
document.addEventListener('readystatechange', function() {
    if (document.readyState === "complete") {
        document.getElementsByTagName("BODY")[0].setAttribute("id", "app");
    }
});

const app = new Vue({
    el: '#report',
    data: {
        transactions: [],
    },

    created() {
        const url = `https://api.stage.capusta.space/v1/cabinet/protected/transactions/page/1`;
        const token = 'Bearer 5d370094-57a7-4476-ace4-4f29bfa43d44';
        const obj = {
            method: 'GET',
            headers: {
                'Authorization': token,
                'Content-Type': 'application/json'
            }
        };

        fetch(url, obj).then(response => response.json()).then(data => {
            this.transactions = this.groupByDate(data.result);
        }).catch(error => console.log(error));
    },

    methods: {
        dateToTime: function(date) {
            const newDate = new Date(date);
            const formattedTime = newDate.toLocaleTimeString(navigator.language, {hour: '2-digit', minute:'2-digit'});

            return formattedTime;
        },
        makeSpaces: function(number) {
            if (number) {
                let numberSpaced = number.match(/.{1,4}/g);

                return numberSpaced.join(' ');
            }
        },
        groupByDate: function(array) {
            const groups = array.reduce((groups, item) => {
                const date = item.created_at.split('T')[0].replace(/-/g, '.');

                if (!groups[date]) {
                    groups[date] = [];
                }

                groups[date].push(item);

                return groups;
            }, {});

            const groupArrays = Object.keys(groups).map((date) => {
                return {
                    date,
                    items: groups[date]
                };
            });

            return groupArrays;
        }
    }
})
